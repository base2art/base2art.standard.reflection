namespace Base2art.Reflection.Discovery
{
    using System;

    public interface IPackagedDll
    {
        string Name { get; }
        string PackageName { get; }
        string VersionName { get; }
        Version Version { get; }
        string FrameworkVersion { get; }
        FrameworkType FrameworkType { get; }

        string RuntimeIdentifier { get; }
        string FullPath { get; }
    }
}