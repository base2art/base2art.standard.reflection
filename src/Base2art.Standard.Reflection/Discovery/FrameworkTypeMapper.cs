namespace Base2art.Reflection.Discovery
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Text;

    public class FrameworkTypeMapper
    {
        private readonly List<KeyValuePair<string, FrameworkType>> frameworkMap;

        public FrameworkTypeMapper() => this.frameworkMap = CreateMap();

        private static List<KeyValuePair<string, FrameworkType>> CreateMap()
        {
            var frameworkMapContainer = new List<KeyValuePair<string, FrameworkType>>();

            void AddByName(Func<int, string> map, FrameworkType type)
            {
                var range = new[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

                foreach (var x in range.Select(map))
                {
                    frameworkMapContainer.Add(new KeyValuePair<string, FrameworkType>(x, type));
                }
            }

            AddByName(x => $"net{x}*", FrameworkType.NetFx);
            AddByName(x => $"netstandard{x}*", FrameworkType.NetStandard);
            AddByName(x => $"netcore{x}*", FrameworkType.NetCore);
            AddByName(x => $"netcoreapp{x}*", FrameworkType.NetCore);
            AddByName(x => $"windowsphone{x}*", FrameworkType.WindowsPhone);
            AddByName(x => $"wpa{x}*", FrameworkType.WindowsPhone);
            AddByName(x => $"wp{x}*", FrameworkType.WindowsPhone);
            AddByName(x => $"sl{x}*", FrameworkType.SilverLight);
            AddByName(x => $"win{x}*", FrameworkType.WindowsDesktop);
            AddByName(x => $"windows{x}*", FrameworkType.WindowsDesktop);
            AddByName(x => $"uap{x}*", FrameworkType.WindowsDesktop);

            frameworkMapContainer.Add(new KeyValuePair<string, FrameworkType>("monoandroid*", FrameworkType.Mono));
            frameworkMapContainer.Add(new KeyValuePair<string, FrameworkType>("monotouch*", FrameworkType.Mono));
            frameworkMapContainer.Add(new KeyValuePair<string, FrameworkType>("monoMac*", FrameworkType.Mono));
            frameworkMapContainer.Add(new KeyValuePair<string, FrameworkType>("Xamarin*", FrameworkType.Xamarin));
            frameworkMapContainer.Add(new KeyValuePair<string, FrameworkType>("portable-*", FrameworkType.Portable));
            frameworkMapContainer.Add(new KeyValuePair<string, FrameworkType>("net", FrameworkType.NetFx));
            frameworkMapContainer.Add(new KeyValuePair<string, FrameworkType>("netcoreapp", FrameworkType.NetCore));
            return frameworkMapContainer;
        }

        public FrameworkType FindFrameworkTypeByName(string frameworkDirName)
        {
            foreach (var pair in this.frameworkMap)
            {
                if (pair.Key.IsWildCardMatch(frameworkDirName, true))
                {
                    return pair.Value;
                }
            }

            return FrameworkType.Unknown;
        }
    }
}