namespace Base2art.Reflection.Discovery
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    internal static class DotnetLocator
    {
        public static DirectoryInfo[] GetPackagesPaths()
        {
            var dirs = new List<DirectoryInfo>();

            var basePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile), ".nuget", "packages");
            if (Directory.Exists(basePath))
            {
                dirs.Add(new DirectoryInfo(basePath));
            }

            var item = FindDotNetDirectories().Select(x => Path.Combine(x.FullName, "sdk", "NuGetFallbackFolder"))
                                              .Where(Directory.Exists)
                                              .Select(x => new DirectoryInfo(x));
            dirs.AddRange(item);

            return dirs.ToArray();
        }

        public static DirectoryInfo FindFirstDotNetPath()
            => FindDotNetDirectories().FirstOrDefault();

        public static DirectoryInfo[] FindDotNetDirectories()
            => new[] {"dotnet", "dotnet.exe"}
               .Select(FindExePath)
               .Where(File.Exists)
               .Select(x => new FileInfo(x))
               .Select(x => x.Directory)
               .ToArray();

        private static string FindExePath(string exe)
        {
            exe = Environment.ExpandEnvironmentVariables(exe);
            if (File.Exists(exe))
            {
                return Path.GetFullPath(exe);
            }

            if (string.IsNullOrWhiteSpace(Path.GetDirectoryName(exe)))
            {
                var envPath = Environment.GetEnvironmentVariable("PATH") ?? "";
                var item = envPath.Split(Path.PathSeparator)
                                  .Select(test => test.Trim())
                                  .Where(path => !string.IsNullOrEmpty(path))
                                  .Select(path => Path.Combine(path, exe))
                                  .Where(File.Exists)
                                  .Select(Path.GetFullPath)
                                  .FirstOrDefault();

                if (!string.IsNullOrWhiteSpace(item))
                {
                    return item;
                }
            }

            return null;
        }
    }
}