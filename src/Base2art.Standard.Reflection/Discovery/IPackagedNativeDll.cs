namespace Base2art.Reflection.Discovery
{
    using System;

    public interface IPackagedNativeDll
    {
        string Name { get; }
        string PackageName { get; }
        string VersionName { get; }
        Version Version { get; }

        string RuntimeIdentifier { get; }
        string FullPath { get; }
    }
}