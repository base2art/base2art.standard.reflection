namespace Base2art.Reflection.Discovery
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Text;

    public class PackagedCodeLibrarySearcher
    {
        private readonly DirectoryInfo[] basePaths;

        private readonly FrameworkTypeMapper frameworkMapper;

        public PackagedCodeLibrarySearcher()
            : this(DotnetLocator.GetPackagesPaths())
        {
        }

        public PackagedCodeLibrarySearcher(string packagesPath)
            : this(Directory.Exists(packagesPath)
                       ? new[] {new DirectoryInfo(packagesPath)}
                       : throw new DirectoryNotFoundException(packagesPath))
        {
        }

        private PackagedCodeLibrarySearcher(DirectoryInfo[] baseUrl)
        {
            this.basePaths = baseUrl;
            this.frameworkMapper = new FrameworkTypeMapper();
        }

        public IReadOnlyList<string> GetAllPackages()
            => this.basePaths.SelectMany(x => x.GetDirectories()).Select(x => x.Name).ToList();

        public (IReadOnlyList<IPackagedDll> Managed, IReadOnlyList<IPackagedNativeDll> Unmanaged) GetLibraries(string packageName)
            => this.GetLibraries(packageName, null);

        public (IReadOnlyList<IPackagedDll> Managed, IReadOnlyList<IPackagedNativeDll> Unmanaged) GetLibraries(
            string packageName, string versionName)
        {
            var managedResults = new List<IPackagedDll>();
            var nativeResults = new List<IPackagedNativeDll>();
            var dirs = this.basePaths.SelectMany(x => x.GetDirectories())
                           .Where(x => string.Equals(x.Name, packageName, StringComparison.OrdinalIgnoreCase));

            foreach (var dir in dirs)
            {
                foreach (var versionDir in dir.GetDirectories())
                {
                    var version = this.ParseVersion(versionDir.Name);
                    if (version != null && version == (this.ParseVersion(versionName) ?? version))
                    {
//                        runtimeIdentifier, /
                        this.ProcessRuntimeDirectories(versionDir, osDir =>
                        {
                            this.AddFromLib(
                                            versionDir,
                                            osDir,
                                            managedResults,
                                            version,
                                            dir,
                                            osDir.Name);

                            this.AddNativeDirectoryToPath(
                                                          nativeResults,
                                                          version,
                                                          versionDir,
                                                          osDir,
                                                          dir);
                        });

                        this.AddFromLib(
                                        versionDir,
                                        null,
                                        managedResults,
                                        version,
                                        dir,
                                        "any");
                    }
                }
            }

            return (managedResults, nativeResults);
        }

        private void AddFromLib(
            DirectoryInfo versionDir,
            DirectoryInfo osDir,
            List<IPackagedDll> results,
            Version version,
            DirectoryInfo dir,
            string architectureType)
        {
            if (osDir == null)
            {
                osDir = versionDir;
            }

            var libDirPath = Path.Combine(osDir.FullName, "lib");
            if (Directory.Exists(libDirPath))
            {
                var libDir = new DirectoryInfo(libDirPath);

                foreach (var frameworkDir in libDir.GetDirectories())
                {
                    foreach (var dll in frameworkDir.GetFiles("*.dll"))
                    {
                        results.Add(new PackagedDll
                                    {
                                        Name = Path.GetFileNameWithoutExtension(dll.Name),
                                        Version = version,
                                        VersionName = versionDir.Name,
                                        FrameworkVersion = frameworkDir.Name,
                                        FrameworkType = this.Parse(frameworkDir.Name),
                                        RuntimeIdentifier = architectureType,
                                        FullPath = dll.FullName,
                                        PackageName = dir.Name
                                    });
                    }
                }
            }
        }

        private void AddNativeDirectoryToPath(
            List<IPackagedNativeDll> natives,
            Version version,
            DirectoryInfo versionDir,
            DirectoryInfo ridDir,
            DirectoryInfo packageDir)
        {
            var items = ridDir.GetDirectories().FirstOrDefault(x => string.Equals(x.Name, "native", StringComparison.OrdinalIgnoreCase));
            if (items != null)
            {
                foreach (var probableNativeDll in items.EnumerateFiles())
                {
                    natives.Add(new PackagedNativeDll
                                {
                                    Name = Path.GetFileNameWithoutExtension(probableNativeDll.Name),
                                    Version = version,
                                    VersionName = versionDir.Name,
                                    RuntimeIdentifier = ridDir.Name,
                                    FullPath = probableNativeDll.FullName,
                                    PackageName = packageDir.Name
                                });
                }
            }
        }

        private FrameworkType Parse(string frameworkDirName)
            => this.frameworkMapper.FindFrameworkTypeByName(frameworkDirName);

        private Version ParseVersion(string versionDirName)
        {
            if (versionDirName == null)
            {
                return null;
            }

            versionDirName = versionDirName.TakeWhile(ch => char.IsDigit(ch) || ch == '.').AsString();
            if (Version.TryParse(versionDirName, out var v))
            {
                return v;
            }

            return null;
        }

        protected void ProcessRuntimeDirectories(
            DirectoryInfo baseDir,
            Action<DirectoryInfo> onMatch)
        {
//            var rids = this.Rids(envRid);

            var runtimesDirPath = Path.Combine(baseDir.FullName, "runtimes");
            if (Directory.Exists(runtimesDirPath))
            {
                var runtimesDir = new DirectoryInfo(runtimesDirPath);
                foreach (var osDir in runtimesDir.GetDirectories())
                {
//                    if (rids.Contains(osDir.Name))
//                    {
                    onMatch(osDir);
//                    }
                }
            }
        }

        private class PackagedDll : IPackagedDll
        {
            public string Name { get; set; }
            public string PackageName { get; set; }
            public string VersionName { get; set; }
            public Version Version { get; set; }
            public string FrameworkVersion { get; set; }
            public FrameworkType FrameworkType { get; set; }
            public string RuntimeIdentifier { get; set; }

            public string FullPath { get; set; }
        }

        private class PackagedNativeDll : IPackagedNativeDll
        {
            public string Name { get; set; }
            public string PackageName { get; set; }
            public string VersionName { get; set; }
            public Version Version { get; set; }
            public string RuntimeIdentifier { get; set; }

            public string FullPath { get; set; }
        }
    }
}

//            var runtimeIdentifier = RuntimeEnvironment.GetRuntimeIdentifier();
//                if (rids.Contains("win"))
//                {
//                    var path = Environment.GetEnvironmentVariable("PATH");
//                    Environment.SetEnvironmentVariable("PATH", $"{path}{Path.PathSeparator}{items.FullName}");
//                }

//                if (rids.Contains("unix") || rids.Contains("linux") || rids.Contains("osx"))
//                {
//                    var ldLibraryPath = Environment.GetEnvironmentVariable("LD_LIBRARY_PATH");
//                    Environment.SetEnvironmentVariable("LD_LIBRARY_PATH", $"{ldLibraryPath}{Path.PathSeparator}{items.FullName}");
//                }

//
//        public IReadOnlyList<IPackagedDll> GetDlls(string packageName) => this.GetDlls(packageName, null);
//
//        public IReadOnlyList<IPackagedDll> GetDlls(string packageName, string versionName)
//            => this.GetManagedAndUnmanagedDlls(packageName, versionName).Managed;