namespace Base2art.Reflection.Discovery
{
    public enum FrameworkType
    {
        Unknown,
        Native,
        NetCore,
        NetStandard,
        Portable,
        NetFx,
        WindowsPhone,
        Xamarin,
        Mono,
        SilverLight,
        WindowsDesktop
    }
}