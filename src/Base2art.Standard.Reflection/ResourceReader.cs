namespace Base2art.Reflection
{
    using System.IO;
    using System.Reflection;

    public static class StreamReader
    {
        public static byte[] ReadResource(this Assembly asm, string resourceName)
        {
            var stream = asm.GetManifestResourceStream(resourceName);

            return stream.ReadFully();
        }

        public static string ReadResourceAsString(this Assembly asm, string resourceName)
        {
            var stream = asm.GetManifestResourceStream(resourceName);

            using (var sr = new System.IO.StreamReader(stream))
            {
                return sr.ReadToEnd();
            }
        }

        public static byte[] ReadFully(this Stream stream)
        {
            if (stream is MemoryStream memoryStream)
            {
                return memoryStream.ToArray();
            }

            // Jon Skeet's accepted answer 
            return StreamToByteArray(stream);
        }

        private static byte[] StreamToByteArray(this Stream input)
        {
            var buffer = new byte[16 * 1024];
            using (var ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }

                return ms.ToArray();
            }
        }
    }
}