using Base2art.Serialization.CodeGeneration;

[assembly: IncludeJsonSerializer("Base2art.Reflection.Json", MakePublic = false)]