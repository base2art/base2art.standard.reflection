namespace Base2art.Reflection.Serialization
{
    using System.Linq;
    using Json;
    using Json.Converters;

    internal static class CustomConverters
    {
        public static IConverter[] AllCustomConverters(this IJsonSerializer serializer)
        {
            return serializer.AllConverters()
                             .Concat(new IConverter[] {new DictionaryObjectObjectConverter(serializer)})
                             .ToArray();
        }
    }
}