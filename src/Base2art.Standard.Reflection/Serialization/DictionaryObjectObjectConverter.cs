namespace Base2art.Reflection.Serialization
{
    using System;
    using Json;
    using Json.Converters;

    /// <summary>
    ///     A Dictionary deserializer.
    /// </summary>
    internal class DictionaryObjectObjectConverter : DictionaryAnyAnyConverterBase
    {
        public DictionaryObjectObjectConverter(IJsonSerializer serializer) : base(serializer)
        {
        }

        protected override bool IsMatch(Type first) => first == typeof(object);
        protected override string SerializeKey(Type objectType, object value) => this.Serializer.SerializePrimitiveValue(value);
        protected override object DeserializeKey(Type objectType, string key) => key;
    }
}