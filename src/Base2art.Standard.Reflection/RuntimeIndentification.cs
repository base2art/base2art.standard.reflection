namespace Base2art.Reflection
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Json;

    public class RuntimeIndentification
    {
        private readonly Lazy<RuntimesFile> method;
        private readonly string runtimeFilePath;

        public RuntimeIndentification(string runtimeFilePath)
        {
            this.runtimeFilePath = runtimeFilePath;
            this.method = new Lazy<RuntimesFile>(this.CreateMapping);
        }

//
        public string[] FindRuntimeDependencies(string id)
            => this.FindRuntimeDependencies(id, true);

        public string[] FindRuntimeDependencies(string id, bool includeSelf)
            => this.FindRuntimeLevelDependencies(id, includeSelf)
                   .Select(x => x.Item1)
                   .ToArray();

        public string[] AllKnownRuntimeIdentifiers()
            => this.method.Value.Runtimes
                   .Select(x => x.Key)
                   .ToArray();

        public List<(string Rid, int Level)> FindRuntimeLevelDependencies(string rid, bool includeSelf)
        {
            var resultRuntimes = this.method.Value.Runtimes;

            var all = new List<Tuple<string, int>>();

            if (includeSelf)
            {
                all.Add(Tuple.Create(rid, 0));
            }

            Process(all, rid, resultRuntimes, 0);

            var items = all.GroupBy(x => x.Item1, StringComparer.OrdinalIgnoreCase)
                           .Select(x => (Rid: x.Key, Level: x.Min(y => y.Item2)))
                           .OrderBy(x => x.Level)
                           .ToList();

            return items;
        }

        private static void Process(
            List<Tuple<string, int>> all,
            string id,
            Dictionary<string, Dictionary<string, List<string>>> resultRuntimes,
            int level)
        {
            var next = resultRuntimes[id]["#import"].ToArray();

            all.AddRange(next.Select(x => Tuple.Create(x, level)));

            foreach (var x in next)
            {
                Process(all, x, resultRuntimes, level + 1);
            }
        }

        private RuntimesFile CreateMapping()
        {
            string GetContent() =>
                File.Exists(this.runtimeFilePath)
                    ? File.ReadAllText(this.runtimeFilePath)
                    : throw new FileNotFoundException(this.runtimeFilePath);

            var content = GetContent();
            var serializer = new CamelCasingIndentedSimpleJsonSerializer();
            var result = serializer.Deserialize<RuntimesFile>(content);

            return result;
        }

        private class RuntimesFile
        {
            public Dictionary<string, Dictionary<string, List<string>>> Runtimes { get; set; }
        }
    }
}