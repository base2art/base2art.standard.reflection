namespace Base2art.Reflection.Activation
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;

    public class FancyActivatorBuilder
    {
        private IDictionary<string, object> inputParameters = new Dictionary<string, object>();

        private IServiceLookup lookup = new ServiceLookup();
        private IDictionary<string, object> properties = new Dictionary<string, object>();

        public FancyActivatorBuilder WithServiceLookup(IServiceLookup value)
        {
            this.lookup = value ?? throw new ArgumentNullException(nameof(value));
            return this;
        }

        public FancyActivatorBuilder WithInputParameters(IDictionary<string, object> setter)
        {
            this.inputParameters = setter ?? throw new ArgumentNullException(nameof(setter));
            return this;
        }

        public FancyActivatorBuilder WithInputParameters(Action<IDictionary<string, object>> setter)
        {
            setter(this.inputParameters);
            return this;
        }

        public FancyActivatorBuilder WithProperties(IDictionary<string, object> setter)
        {
            this.properties = setter ?? throw new ArgumentNullException(nameof(setter));
            return this;
        }

        public FancyActivatorBuilder WithProperties(Action<IDictionary<string, object>> setter)
        {
            setter(this.properties);
            return this;
        }

        public object Create(Type type)
            => type.CreateFrom(this.lookup, this.Wrap(this.inputParameters), this.Wrap(this.properties));

        public T CreateAs<T>()
            => this.lookup.CreateFrom<T>(this.Wrap(this.inputParameters), this.Wrap(this.properties));

        private IReadOnlyDictionary<string, object> Wrap(IDictionary<string, object> dictionary)
            => new ReadOnlyDictionary<string, object>(dictionary ?? new Dictionary<string, object>());
    }
}