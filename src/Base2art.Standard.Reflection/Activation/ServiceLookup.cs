namespace Base2art.Reflection.Activation
{
    using System;

    internal class ServiceLookup : IServiceLookup
    {
        public object[] ResolveAll(Type contractType) => new object[0];

        public object Resolve(Type contractType, bool returnDefaultOnNotFound)
            => null;

        public Type PrimaryException { get; } = typeof(InvalidCastException);

        public Exception Rethrow(string errorMessage, Exception inner)
            => new InvalidCastException(errorMessage, inner);
    }
}