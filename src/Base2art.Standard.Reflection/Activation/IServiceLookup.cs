namespace Base2art.Reflection.Activation
{
    using System;

    public interface IServiceLookup
    {
        Type PrimaryException { get; }

        /// <summary>Resolves a list of objects by type.</summary>
        /// <param name="contractType">The type of objects to return.</param>
        /// <returns>The list of resolved object.</returns>
        object[] ResolveAll(Type contractType);

//        /// <summary>Resolves a list of objects by type.</summary>
//        /// <typeparam name="T">The type of objects to return.</typeparam>
//        /// <returns>The list of resolved object.</returns>
//        T[] ResolveAll<T>();

        /// <summary>Resolves an object by type.</summary>
        /// <param name="contractType">The type of object to return.</param>
        /// <param name="returnDefaultOnNotFound">
        ///     A value indicating that a null value should be returned (as opposed to throwing an exception)
        ///     when resolution fails.
        /// </param>
        /// <returns>The resolved object.</returns>
        object Resolve(Type contractType, bool returnDefaultOnNotFound);

        Exception Rethrow(string errorMessage, Exception inner);
    }
}