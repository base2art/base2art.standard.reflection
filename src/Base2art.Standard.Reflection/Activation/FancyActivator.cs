namespace Base2art.Reflection.Activation
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using Serialization;

    public static class FancyActivator
    {
        public static T CreateFrom<T>(
            this IServiceLookup config,
            IReadOnlyDictionary<string, object> inputParms,
            IReadOnlyDictionary<string, object> properties)
        {
            if (inputParms == null)
            {
                inputParms = new Dictionary<string, object>();
            }

            if (properties == null)
            {
                properties = new Dictionary<string, object>();
            }

            if (config == null)
            {
                config = new ServiceLookup();
            }

            var obj = (T) CreateFromInternal(typeof(T), config, inputParms);

            SetProperties(properties, obj, config);
            return obj;
        }

        public static object CreateFrom(
            this Type type,
            IServiceLookup config,
            IReadOnlyDictionary<string, object> inputParms,
            IReadOnlyDictionary<string, object> properties)
        {
            if (inputParms == null)
            {
                inputParms = new Dictionary<string, object>();
            }

            if (properties == null)
            {
                properties = new Dictionary<string, object>();
            }

            if (config == null)
            {
                config = new ServiceLookup();
            }

            var obj = CreateFromInternal(type, config, inputParms);

            SetProperties(properties, obj, config);
            return obj;
        }

        private static object CreateFromInternal(this Type t, IServiceLookup config, IReadOnlyDictionary<string, object> inputParms)
        {
            var ctors = t.GetConstructors();
            if (ctors.Length == 0)
            {
                return Activator.CreateInstance(t);
            }

            var items = (from x in ctors
                         orderby x.GetParameters().Length
                         select x).ToList();
            var ctor = items[0];
            var parms = ctor.GetParameters();
            var length = parms.Length;
            if (length == 0)
            {
                return Activator.CreateInstance(t);
            }

            var obj = new object[length];
            for (var i = 0; i < obj.Length; i++)
            {
                var parameterInfo = parms[i];

                obj[i] = GetValueFromValue(config, parameterInfo, t, p => p.Name, p => p.ParameterType, inputParms, true);
            }

            return Activator.CreateInstance(t, obj);
        }

        private static bool IsNullableValueType(this Type type) => Nullable.GetUnderlyingType(type) != null;

        private static bool IsConcreteType(this Type type) => !type.IsAbstract && !type.IsInterface;

        private static void SetProperties(IReadOnlyDictionary<string, object> properties, object obj, IServiceLookup config)
        {
            if (!(properties?.Count > 0))
            {
                return;
            }

            var objType = obj.GetType();
            foreach (var element in properties)
            {
                var prop = objType.GetProperty(element.Key);
                var propSet = prop.GetSetMethod();

                var value = GetValueFromValue(config, prop, objType, x => x.Name, x => x.PropertyType, properties, false);

                propSet.Invoke(obj, new[]
                                    {
                                        value
                                    });
            }
        }

        private static object GetValueFromValue<T>(
            IServiceLookup lookup,
            T prop,
            Type declaringType,
            Func<T, string> nameLookup,
            Func<T, Type> typeLookup,
            IReadOnlyDictionary<string, object> values,
            bool checks)
//            where T : MemberInfo
        {
            var type = typeLookup(prop);
            var name = nameLookup(prop);

            if (values.ContainsKey(name))
            {
                return Convert(type, values[name]);
            }

            if (checks)
            {
                if (type == typeof(string))
                {
                    throw new MissingFieldException(type.FullName, name);
                }

                if (type.IsPrimitive)
                {
                    throw new MissingFieldException(type.FullName, name);
                }

                if (type.IsNullableValueType())
                {
                    return null;
//                    throw new MissingFieldException(type.FullName, name);
                }
            }

            try
            {
                if (type.IsArray)
                {
                    var innerParmType = type.GetElementType();

                    return lookup.ResolveAll(innerParmType);
                }

                var service = lookup.Resolve(type, true);
                if (service == null && type.IsConcreteType())
                {
                    return type.CreateFrom(lookup, new Dictionary<string, object>(), new Dictionary<string, object>());
                }

                return service;
            }
            catch (Exception e)
            {
                if (lookup.PrimaryException.IsInstanceOfType(e))
                {
                    var errorMessage = $"{type} in {name}, of class {declaringType.Name}";

                    throw lookup.Rethrow(errorMessage, e);
                }

                throw;
            }
        }

        private static object Convert(Type type, object value)
        {
            var converter = TypeDescriptor.GetConverter(type);
            if (value == null)
            {
                return null;
            }

            if (converter.CanConvertFrom(value.GetType()))
            {
                return converter.ConvertFrom(value);
            }

            var serializer = new CustomJsonSerializer();
            var objString = serializer.Serialize(value);
            return serializer.Deserialize(objString, type);
        }
    }
}