namespace Base2art.Reflection.Features
{
    using System;
    using Discovery;
    using FluentAssertions;
    using Xunit;

    public class DictionaryLoaderFeature
    {
        [Fact]
        public void ShouldLoadDirectory()
        {
            if (Environment.OSVersion.Platform == PlatformID.Win32NT)
            {
                DotnetLocator.FindFirstDotNetPath().FullName.Should().Be(@"C:\Program Files\dotnet");
            }
            else
            {
                DotnetLocator.FindFirstDotNetPath().FullName.Should().Be("/usr/share/dotnet");
            }
        }

        [Fact]
        public void ShouldLoadPackagesPaths()
        {
            var items = DotnetLocator.GetPackagesPaths();
            if (Environment.OSVersion.Platform == PlatformID.Win32NT)
            {
                items[0].FullName.Should().BeEquivalentTo($"C:\\Users\\{Environment.UserName}\\.nuget\\packages");
                items[1].FullName.Should().BeEquivalentTo(@"C:\Program Files\dotnet\sdk\NuGetFallbackFolder");
            }
            else
            {
                items[0].FullName.Should().Be($"/home/{Environment.UserName.ToLowerInvariant()}/.nuget/packages");
                items[1].FullName.Should().Be(@"/usr/share/dotnet/sdk/NuGetFallbackFolder");
            }
        }

        [Fact]
        public void ShouldLoadPackagesPaths1()
        {
            var items = new PackagedCodeLibrarySearcher();
            var packages = items.GetAllPackages();
            foreach (var package in packages)
            {
                var dlls = items.GetLibraries(package);
                foreach (var dll in dlls.Managed)
                {
//                    Console.WriteLine(dll.arc);
                }
            }
        }
    }
}