namespace Base2art.Reflection.Features
{
    using System;
    using Xunit.Abstractions;

    public class ExceptionFeatures
    {
        private readonly ITestOutputHelper helper;
        public ExceptionFeatures(ITestOutputHelper helper) => this.helper = helper;

//        [Theory]
//        [InlineData(typeof(OutOfMemoryException), true)]
//        [InlineData(typeof(DivideByZeroException), false)]
//        public void ShouldTryCatch(Type exception, bool throwOver)
//        {
//            Action x = () => this.Throw(exception);
//
//            Action y = () => Exceptions.SwallowUserExceptions(x);
//
//            if (throwOver)
//            {
//                Assert.Throws(exception, y);
//            }
//            else
//            {
//                y();
//            }
//        }

        private void Throw(Type exception)
        {
            throw (Exception) Activator.CreateInstance(exception);
        }

//        [Fact]
//        public void ShouldPrintStrings()
//        {
////            var path = @"C:\Program Files\dotnet\shared\Microsoft.NETCore.App\2.0.9\System.Private.CoreLib.dll";
////            var asm = Assembly.LoadFile(path);
//
//            var exportedTypes = typeof(string).Assembly
//                                              .GetExportedTypes()
//                                              .Where(x => typeof(Exception).IsAssignableFrom(x))
//                                              .Where(x => !Exceptions.KnownExceptions.Any(y => y.IsAssignableFrom(x)))
//                                              .ToArray();
//
//            foreach (var exportedType in exportedTypes.Where(x => typeof(SystemException).IsAssignableFrom(x)))
//            {
//                this.helper.WriteLine(exportedType.FullName + " -: " + exportedType.BaseType.FullName);
//            }
//
//            this.helper.WriteLine("");
//            this.helper.WriteLine("");
//            this.helper.WriteLine("");
//
//            foreach (var exportedType in exportedTypes.Where(x => !typeof(SystemException).IsAssignableFrom(x)))
//            {
//                this.helper.WriteLine(exportedType.FullName + " -: " + exportedType.BaseType.FullName);
//            }
//        }
    }
}