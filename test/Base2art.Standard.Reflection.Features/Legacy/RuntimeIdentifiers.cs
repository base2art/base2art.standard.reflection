//namespace Base2art.Reflection
//{
//    using System;
//    using System.Collections.Generic;
//    using System.IO;
//    using System.Linq;
//    using System.Reflection;
//    using Json;
//    using Microsoft.DotNet.PlatformAbstractions;
//
//    public static class RuntimeIdentifiers
//    {
//        private static readonly Lazy<RuntimesFile> Method;
//
//        static RuntimeIdentifiers()
//        {
//            Method = new Lazy<RuntimesFile>(() =>
//            {
//                string GetContent()
//                {
//                    var runtimePath = Path.Combine(Environment.CurrentDirectory, "runtime-tree.json");
//                    return File.Exists(runtimePath)
//                               ? File.ReadAllText(runtimePath)
//                               : Assembly.GetExecutingAssembly().ReadResourceAsString("Base2art.Reflection.Features.runtime.json");
//                }
//
//                var content = GetContent();
//                var serializer = new CamelCasingIndentedSimpleJsonSerializer();
//                var result = serializer.Deserialize<RuntimesFile>(content);
//
//                return result;
//            });
//        }
//
//        public static string[] FindRuntimeDependencies()
//            => FindRuntimeDependencies(true);
//
//        public static string[] FindRuntimeDependencies(bool includeSelf)
//            => FindRuntimeLevelDependencies(RuntimeEnvironment.GetRuntimeIdentifier(), includeSelf)
//               .Select(x => x.Item1)
//               .ToArray();
//
//        public static string[] FindRuntimeDependencies(string id)
//            => FindRuntimeDependencies(id, true);
//
//        public static string[] FindRuntimeDependencies(string id, bool includeSelf)
//            => FindRuntimeLevelDependencies(id, includeSelf)
//               .Select(x => x.Item1)
//               .ToArray();
//
//        public static string[] AllKnownRuntimeIdentifiers()
//            => Method.Value.Runtimes
//                     .Select(x => x.Key)
//                     .ToArray();
//
//        public static List<Tuple<string, int>> FindRuntimeLevelDependencies(string rid, bool includeSelf)
//        {
//            var resultRuntimes = Method.Value.Runtimes;
//
//            var all = new List<Tuple<string, int>>();
//
//            if (includeSelf)
//            {
//                all.Add(Tuple.Create(rid, 0));
//            }
//
//            Process(all, rid, resultRuntimes, 0);
//
//            var items = all.GroupBy(x => x.Item1, StringComparer.OrdinalIgnoreCase)
//                           .Select(x => Tuple.Create(x.Key, x.Min(y => y.Item2)))
//                           .OrderBy(x => x.Item2)
//                           .ToList();
//
//            return items;
//        }
//
//        private static void Process(
//            List<Tuple<string, int>> all,
//            string id,
//            Dictionary<string, Dictionary<string, List<string>>> resultRuntimes,
//            int level)
//        {
//            var next = resultRuntimes[id]["#import"].ToArray();
//
//            all.AddRange(next.Select(x => Tuple.Create(x, level)));
//
//            foreach (var x in next)
//            {
//                Process(all, x, resultRuntimes, level + 1);
//            }
//        }
//
//        private class RuntimesFile
//        {
//            public Dictionary<string, Dictionary<string, List<string>>> Runtimes { get; set; }
//        }
//    }
//}

