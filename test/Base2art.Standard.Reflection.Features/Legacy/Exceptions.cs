//namespace Base2art.Reflection
//{
//    using System;
//    using System.Collections.Generic;
//    using System.Collections.Immutable;
//    using System.IO;
//    using System.Linq;
//    using System.Reflection;
//    using System.Resources;
//    using System.Runtime.InteropServices;
//    using System.Runtime.Serialization;
//    using System.Threading;
//    using System.Threading.Tasks;
//
//    public static class Exceptions
//    {
//        public static ISet<Type> RuntimeExceptions => new HashSet<Type>
//                                                      {
//                                                          typeof(OutOfMemoryException),
//                                                          typeof(InsufficientMemoryException),
//                                                          typeof(BadImageFormatException),
//                                                          typeof(ExecutionEngineException),
//                                                          typeof(InvalidProgramException),
//                                                          typeof(StackOverflowException)
//                                                      };
//
//        public static ISet<Type> CompilationExceptions => new HashSet<Type>
//                                                          {
//                                                              typeof(MemberAccessException),
//                                                              typeof(TypeLoadException),
//                                                              typeof(ArrayTypeMismatchException),
//                                                              typeof(AccessViolationException),
//                                                              typeof(UnauthorizedAccessException),
//                                                              typeof(MissingManifestResourceException),
//                                                              typeof(MissingSatelliteAssemblyException),
//                                                              typeof(AmbiguousMatchException),
//                                                              typeof(ReflectionTypeLoadException)
//                                                          };
//
//        public static ISet<Type> DeveloperExceptions => new HashSet<Type>
//                                                        {
//                                                            typeof(KeyNotFoundException),
//                                                            typeof(InvalidCastException),
//                                                            typeof(NotImplementedException),
//                                                            typeof(NotSupportedException),
//                                                            typeof(NullReferenceException),
//                                                            typeof(InvalidOperationException),
//                                                            typeof(TypeInitializationException),
//                                                            typeof(TypeUnloadedException),
//                                                            typeof(IOException),
//                                                            typeof(SerializationException),
//
//                                                            typeof(ExternalException),
//                                                            typeof(ThreadInterruptedException),
//                                                            typeof(AbandonedMutexException),
//                                                            typeof(SemaphoreFullException),
//                                                            typeof(SynchronizationLockException),
//                                                            typeof(ThreadAbortException),
//                                                            typeof(ThreadStartException),
//                                                            typeof(ThreadStateException),
//                                                            typeof(TaskCanceledException)
//                                                        };
//
//        public static ISet<Type> UserExceptions => new HashSet<Type>
//                                                   {
//                                                       typeof(ArgumentException),
//                                                       typeof(ArithmeticException),
//                                                       typeof(FormatException),
//                                                       typeof(IndexOutOfRangeException)
//                                                   };
//
//        public static ISet<Type> KnownExceptions => RuntimeExceptions.Union(CompilationExceptions)
//                                                                     .Union(UserExceptions)
//                                                                     .Union(DeveloperExceptions)
//                                                                     .ToImmutableHashSet();
//
//        public static void SwallowUserExceptions(Action action)
//        {
//            try
//            {
//                action();
//            }
//            catch (Exception e)
//            {
//                if (RuntimeExceptions.Contains(e.GetType()))
//                {
//                    throw;
//                }
//            }
//        }
//    }
//}

