namespace Base2art.Reflection.Features
{
    using System;
    using System.IO;
    using System.Linq;
    using Discovery;
    using FluentAssertions;
    using Microsoft.DotNet.PlatformAbstractions;
    using Microsoft.Win32;
    using Xunit;
    using Xunit.Abstractions;

    public class RidFeature
    {
        public RidFeature(ITestOutputHelper helper) => this.helper = helper;

        private readonly ITestOutputHelper helper;

        [Theory]
        [InlineData("fedora.28-x64", 10)]
        [InlineData("fedora.29-x64", 10)]
        [InlineData("win10-x64", 12)]
        [InlineData("win81-x64", 10)]
        public void ShouldLoadFedora(string rid, int expectedCount)
        {
            var rids = new RuntimeIndentification(this.DefaultPath).FindRuntimeDependencies(rid);
            rids.Length.Should().Be(expectedCount);
        }

        public string DefaultPath
        {
            get
            {
                // /home/tyoung/.nuget/packages/microsoft.netcore.platforms/2.2.3/runtime.json
                return DotnetLocator.GetPackagesPaths()
                                    .Select(x => Path.Combine(
                                                              x.FullName,
                                                              "microsoft.netcore.platforms",
                                                              "2.1.2",
                                                              "runtime.json"))
                                    .Where(File.Exists)
                                    .FirstOrDefault();
            }
        }

        [Theory]
        [InlineData("fedora.28-x64")]
        [InlineData("win10-x64")]
        [InlineData("win81-x64")]
        [InlineData("osx.10.12-x64")]
        public void ShouldPrint(string rid)
        {
            var rids = new RuntimeIndentification(this.DefaultPath).FindRuntimeDependencies(rid);
            foreach (var ridItem in rids)
            {
                this.helper.WriteLine(ridItem);
            }
        }

        private bool IsWindows10()
            => this.ProductName().StartsWith("Windows 10");

        private bool IsWindowsServer2012R2Standard()
            => this.ProductName().StartsWith("Windows Server 2012 R2 Standard");

        private string ProductName()
        {
            try
            {
                var reg = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows NT\CurrentVersion");
                var productName = (string) reg.GetValue("ProductName");
                this.helper.WriteLine(productName);
                return productName;
            }
            catch (TypeInitializationException e)
            {
                if (e.InnerException is PlatformNotSupportedException)
                {
                    return "NOT WINDOWS {21EAFC1E-F2D8-4F8F-B161-64AFC39D6CE4}";
                }

                throw;
            }
        }

        [Fact]
        public void ShouldLoad()
        {
            var rid = RuntimeEnvironment.GetRuntimeIdentifier();
            var rids = new RuntimeIndentification(this.DefaultPath).FindRuntimeDependencies(rid, true);

            if (this.IsWindows10() && Environment.Is64BitOperatingSystem)
            {
                rids[0].Should().Be("win10-x64");
                rids.Should().HaveCount(12);
                return;
            }

            if (this.IsWindowsServer2012R2Standard() && Environment.Is64BitOperatingSystem)
            {
                rids[0].Should().Be("win81-x64");
                rids.Should().HaveCount(10);
            }

#if _WINDOWS
            throw new ArgumentOutOfRangeException();
#endif
            // 
            //            rids[0].Should().Be("win10-x64");
            //            rids.Should().HaveCount(12);
        }

        [Fact]
        public void ShouldLoadAll()
        {
            var rids = new RuntimeIndentification(this.DefaultPath).AllKnownRuntimeIdentifiers();
            rids.Length.Should().Be(515);
        }
    }
}