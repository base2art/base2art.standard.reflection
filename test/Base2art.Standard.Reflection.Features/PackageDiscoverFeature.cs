namespace Base2art.Reflection.Features
{
    using System;
    using System.ComponentModel;
    using System.IO;
    using System.IO.Compression;
    using System.Linq;
    using System.Reflection;
    using Discovery;
    using FluentAssertions;
    using Xunit;
    using Xunit.Abstractions;

    public class PackageDiscoverFeature : Component
    {
        public PackageDiscoverFeature(ITestOutputHelper output)
        {
            this.output = output;
            var executingAssembly = Assembly.GetExecutingAssembly();
            var bytes = executingAssembly.ReadResource("Base2art.Reflection.Features.package-test-content.zip");

            this.path = Path.GetTempFileName();
            File.WriteAllBytes(this.path, bytes);
            this.dir = Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString("N").Substring(12));
            ZipFile.ExtractToDirectory(this.path, this.dir);

            this.output.WriteLine(this.path);
            this.output.WriteLine(this.dir);
        }

        private readonly ITestOutputHelper output;
        private readonly string path;
        private readonly string dir;

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if (File.Exists(this.path))
            {
                File.Delete(this.path);
            }

            if (Directory.Exists(this.dir))
            {
                try
                {
                    Directory.Delete(this.dir, true);
                }
                catch (Exception)
                {
                }
            }
        }

        // TODO: MACHINE DEPENDANT...
        [Theory]
        [InlineData("NETStandard.Library", 0)]
        [InlineData("swashbuckle.aspnetcore.swaggerui", 6)]
        [InlineData("Swashbuckle.AspNetCore.SwaggerUI", 6)]
        [InlineData("BD6AFD6F00384218A2BE7EA8D40011E8", 0)]
        [InlineData("base2art.standard.dapper", 2)]
        [InlineData("Base2art.DataStorage.Provided", 3)]
        public void Test1(string packageName, int expectedManagedCount)
        {
            var packageDllLoader = new PackagedCodeLibrarySearcher(this.dir);
            packageDllLoader.GetLibraries(packageName).Managed.Count.Should().Be(expectedManagedCount);
        }

        [Theory]
        [InlineData(true, "NETStandard.Library", 0, 0)]
        [InlineData(true, "swashbuckle.aspnetcore.swaggerui", 6, 0)]
        [InlineData(true, "Swashbuckle.AspNetCore.SwaggerUI", 6, 0)]
        [InlineData(true, "BD6AFD6F00384218A2BE7EA8D40011E8", 0, 0)]
        [InlineData(true, "base2art.standard.dapper", 2, 0)]
        [InlineData(true, "Base2art.DataStorage.Provided", 3, 0)]
        [InlineData(true, "sqlitepclraw.provider.e_sqlite3.netstandard11", 3, 0)]
        [InlineData(true, "sqlitepclraw.lib.e_sqlite3.linux", 0, 16)]
        public void Test2(bool usePredifinedData, string packageName, int expectedManagedCount, int expectedUnmanagedCount)
        {
            var packageDllLoader = new PackagedCodeLibrarySearcher(usePredifinedData ? this.dir : "/home/tyoung/.nuget/packages");
            var dlls = packageDllLoader.GetLibraries(packageName);
            dlls.Managed.Count.Should().Be(expectedManagedCount);
            dlls.Unmanaged.Count.Should().Be(expectedUnmanagedCount);
            foreach (var nativeDll in dlls.Unmanaged)
            {
                this.output.WriteLine(nativeDll.FullPath);
            }
        }

//        [Fact]
//        public void ShouldLoadSqlClient()
//        {
//            var packageDllLoader = new PackageDllLoader();
//            
//            var items = packageDllLoader.GetDlls("System.Data.SqlClient", "4.5.1");
//            items.Count.Should().Be(28);
////            items.Where(x => x.RuntimeIdentifier == "unix").Count().Should().Be(0);
//            items.Where(x => x.RuntimeIdentifier == "win").Count().Should().Be(14);
//            items.Where(x => x.RuntimeIdentifier == "any").Count().Should().Be(14);
//        }

        [Fact]
        public void FindMatchingDirectories()
        {
            var packageDllLoader = new PackagedCodeLibrarySearcher();
            var packages = packageDllLoader.GetAllPackages();

            var frameworks = packages.SelectMany(packageName => packageDllLoader.GetLibraries(packageName).Managed)
                                     .GroupBy(x => x.FrameworkType);

            foreach (var framework in frameworks)
            {
                this.output.WriteLine("");
                this.output.WriteLine(framework.Key.ToString("G"));
                var fwvs = framework.Select(x => x.FrameworkVersion)
                                    .Distinct(StringComparer.OrdinalIgnoreCase)
                                    .OrderBy(x => x, StringComparer.OrdinalIgnoreCase)
                                    .ToArray();

                Array.ForEach(fwvs, x => this.output.WriteLine($"  {x}"));

                if (framework.Key == FrameworkType.Unknown)
                {
                    foreach (var packagedDll in framework)
                    {
                        this.output.WriteLine(packagedDll.FullPath);
                    }
                }
            }
        }

        [Fact]
        public void GetAll()
        {
            var packageDllLoader = new PackagedCodeLibrarySearcher();
            var packages = packageDllLoader.GetAllPackages();

            var frameworks = packages.SelectMany(packageName => packageDllLoader.GetLibraries(packageName).Managed)
                                     .GroupBy(x => x.FrameworkType);

            foreach (var framework in frameworks)
            {
                this.output.WriteLine("");
                this.output.WriteLine(framework.Key.ToString("G"));
                var fwvs = framework.Select(x => x.FrameworkVersion)
                                    .Distinct(StringComparer.OrdinalIgnoreCase)
                                    .OrderBy(x => x, StringComparer.OrdinalIgnoreCase)
                                    .ToArray();

                Array.ForEach(fwvs, x => this.output.WriteLine($"  {x}"));

                if (framework.Key == FrameworkType.Unknown)
                {
                    foreach (var packagedDll in framework)
                    {
                        this.output.WriteLine(packagedDll.FullPath);
                    }
                }
            }
        }

        [Fact]
        public void ShouldVerifyManagedPackage()
        {
            var packageDllLoader = new PackagedCodeLibrarySearcher(this.dir);
            var dlls = packageDllLoader.GetLibraries("base2art.standard.dapper");
            var managed = dlls.Managed;

            managed.Count.Should().Be(2);
            dlls.Unmanaged.Count.Should().Be(0);
            managed[0].Name.Should().Be("Base2art.Standard.Dapper");
            managed[0].Version.Should().Be(new Version("0.0.0.1"));
            managed[0].FrameworkType.Should().Be(FrameworkType.NetStandard);
            managed[0].FrameworkVersion.Should().Be("netstandard1.3");
            managed[0].FullPath.Should().Be(Path.Combine(
                                                         this.dir,
                                                         "base2art.standard.dapper",
                                                         "0.0.0.1",
                                                         "lib",
                                                         "netstandard1.3",
                                                         "Base2art.Standard.Dapper.dll"));
            managed[0].PackageName.Should().Be("base2art.standard.dapper");
            managed[0].RuntimeIdentifier.Should().Be("any");
            managed[0].VersionName.Should().Be("0.0.0.1");

            managed[1].VersionName.Should().Be("0.0.0.1-beta1");
            managed[1].Version.Should().Be(new Version("0.0.0.1"));
        }

//        [Fact]
//        public void ShouldLoadDapper2()
//        {
//            var packageDllLoader = new PackagedCodeLibrarySearcher();
//            var dlls = packageDllLoader.GetLibraries("microsoft.data.sqlclient");
//            var items = dlls.Managed.Where(x => x.VersionName == "2.0.0").ToArray();
//            items.Length.Should().Be(11);
//            var pathOf = "microsoft.data.sqlclient/2.0.0/runtimes/win/lib/netcoreapp2.1/Microsoft.Data.SqlClient.dll";
//            items.Any(x => x.FullPath.EndsWith(pathOf))
//                 .Should().BeTrue();
//
//            var dll = items.First(x => x.FullPath.EndsWith(pathOf));
//            dll.Name.Should().Be("");
//
////            var rids = new RuntimeIndentification(this.DefaultPath).FindRuntimeDependencies(rid);
////            rids.Length.Should().Be(expectedCount);
//        }

        [Fact]
        public void ShouldVerifySqlClientPackage()
        {
            var packageDllLoader = new PackagedCodeLibrarySearcher(this.dir);
            var dlls = packageDllLoader.GetLibraries("microsoft.data.sqlclient");

            dlls.Managed.Count.Should().Be(8);
            dlls.Unmanaged.Count.Should().Be(0);

            var readOnlyList = dlls.Managed
                                   .Where(x => x.RuntimeIdentifier != "any")
                                   .OrderBy(x => x.Version)
                                   .ThenBy(x => x.RuntimeIdentifier)
                                   .ToArray();

            readOnlyList[0].Name.Should().Be("Microsoft.Data.SqlClient");
            readOnlyList[0].Version.Should().Be(new Version("1.1.0"));
            readOnlyList[0].RuntimeIdentifier.Should().Be("unix");
            readOnlyList[0].VersionName.Should().Be("1.1.0");

            // microsoft.data.sqlclient/1.1.0/runtimes/unix/lib/netcoreapp2.1/
            readOnlyList[0].FullPath.Should().Be(Path.Combine(
                                                              this.dir,
                                                              "microsoft.data.sqlclient",
                                                              "1.1.0",
                                                              "runtimes",
                                                              "unix",
                                                              "lib",
                                                              "netstandard2.0",
                                                              "Microsoft.Data.SqlClient.dll"));
            readOnlyList[0].PackageName.Should().Be("microsoft.data.sqlclient");
        }

        [Fact]
        public void ShouldVerifyUnmanagedPackage()
        {
            var packageDllLoader = new PackagedCodeLibrarySearcher(this.dir);
            var dlls = packageDllLoader.GetLibraries("sqlitepclraw.lib.e_sqlite3.linux");

            dlls.Managed.Count.Should().Be(0);
            dlls.Unmanaged.Count.Should().Be(16);

            var readOnlyList = dlls.Unmanaged.OrderBy(x => x.Version)
                                   .ThenBy(x => x.RuntimeIdentifier)
                                   .ToArray();

            readOnlyList[0].Name.Should().Be("libe_sqlite3");
            readOnlyList[0].Version.Should().Be(new Version("1.1.7"));
            readOnlyList[0].RuntimeIdentifier.Should().Be("linux-x64");
            readOnlyList[0].VersionName.Should().Be("1.1.7");

            readOnlyList[0].FullPath.Should().Be(Path.Combine(
                                                              this.dir,
                                                              "sqlitepclraw.lib.e_sqlite3.linux",
                                                              "1.1.7",
                                                              "runtimes",
                                                              "linux-x64",
                                                              "native",
                                                              "libe_sqlite3.so"));
            readOnlyList[0].PackageName.Should().Be("sqlitepclraw.lib.e_sqlite3.linux");
        }
    }
}