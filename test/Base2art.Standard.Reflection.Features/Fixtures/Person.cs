namespace Base2art.Reflection.Features.Fixtures
{
    public class Person<T>
    {
        public Person(T id) => this.Id = id;

        public T Id { get; }
    }
}