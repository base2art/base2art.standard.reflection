namespace Base2art.Reflection.Features.Fixtures
{
    public class CalendarPublisher
    {
//        private readonly CalendarPublisherSettings settings;

        public CalendarPublisher(CalendarPublisherSettings settings) => this.Settings = settings;

        public CalendarPublisherSettings Settings { get; }
    }
}