namespace Base2art.Reflection.Features.Fixtures
{
    public class CalendarPublisherSettings
    {
        public int Value { get; set; } = 42;
    }
}