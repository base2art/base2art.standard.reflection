namespace Base2art.Reflection.Features
{
    using System;
    using System.Collections.Generic;
    using Activation;
    using Fixtures;
    using FluentAssertions;
    using Xunit;

    public class FancyActivatorFeatures
    {
        [Fact]
        public void ShouldCreateWithConcrete()
        {
            var builder = new FancyActivatorBuilder();
            var person = (CalendarPublisher) builder.Create(typeof(CalendarPublisher));
            person.Settings.Value.Should().Be(42);
        }

        [Fact]
        public void ShouldCreateWithConcreteWithParameterList()
        {
            var builder = new FancyActivatorBuilder();
            var person = (CalendarPublisher) builder.WithInputParameters(x => x["settings"] = new Dictionary<string, string>
                                                                                              {
                                                                                                  {"Value", "41"}
                                                                                              })
                                                    .Create(typeof(CalendarPublisher));
            person.Settings.Value.Should().Be(41);
        }

        [Fact]
        public void ShouldCreateWithConcreteWithParameters()
        {
            var builder = new FancyActivatorBuilder();
            var person = (CalendarPublisher) builder.WithInputParameters(x => x["settings"] = new Dictionary<string, string>
                                                                                              {
                                                                                                  {"Value", "41"}
                                                                                              })
                                                    .Create(typeof(CalendarPublisher));
            person.Settings.Value.Should().Be(41);
        }

        [Fact]
        public void ShouldLoadDictionary()
        {
            var builder = new FancyActivatorBuilder();
            var person = (CalendarPublisher) builder.WithInputParameters(x => x["settings"] = new Dictionary<object, object>
                                                                                              {
//                                                                                                  {new Faker("Value"), "41"}
                                                                                                  {"Value", "41"}
                                                                                              })
                                                    .Create(typeof(CalendarPublisher));

            person.Settings.Value.Should().Be(41);
        }

        [Fact]
        public void ShouldLoadDirectoryGeneric()
        {
            var builder = new FancyActivatorBuilder();
            var person = builder.WithInputParameters(x => x["id"] = "1").CreateAs<Person<int>>();
            person.Id.Should().Be(1);
        }

        [Fact]
        public void ShouldLoadDirectoryNonGeneric()
        {
            {
                var builder = new FancyActivatorBuilder();
                var person = (Person<int>) builder.WithInputParameters(x => x["id"] = "1").Create(typeof(Person<int>));
                person.Id.Should().Be(1);
            }

            {
                var builder = new FancyActivatorBuilder();
                var id = Guid.NewGuid();
                var person = (Person<Guid>) builder.WithInputParameters(x => x["id"] = id.ToString("N")).Create(typeof(Person<Guid>));
                person.Id.Should().Be(id);
            }

            {
                var builder = new FancyActivatorBuilder();
                var id = new[] {"SjY", "mJy"};
                var person = (Person<List<string>>) builder.WithInputParameters(x => x["id"] = id).Create(typeof(Person<List<string>>));
                person.Id.Should().HaveCount(2).And.Subject.Should().HaveElementAt(0, "SjY").And.Subject.Should().HaveElementAt(1, "mJy");
            }

//            {
//                var builder = new FancyActivatorBuilder();
//                var id = "['SjY', 'mJy']";
//                var person = (Person<List<string>>) builder.WithInputParameters(x => x["id"] = id).Create(typeof(Person<List<string>>));
//                person.Id.Should().HaveCount(2).And.Subject.Should().HaveElementAt(0, "SjY").And.Subject.Should().HaveElementAt(1, "mJy");
//            }
        }
    }
}